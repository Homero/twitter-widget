'use strict';
$(document).ready( function () {

    var refreshIntervalId;
    var lastTweetId = 0;
    var watcherStatus = false;

    var searchTweets = function() {
        // Content of textfield needs to be read again since setinterval function won't allow parameters
        var $name = $('#searchtxt').val();
        console.log($name);
        $.get('http://localhost:8080/load',{
            q:$name,
            since_id: lastTweetId
        },function (data,status) {
            // do something with the info
            if (status == "success"){
                if(data.length > 0){
                    lastTweetId = data[0].id;
                    console.log(status);

                    data.reverse().forEach(function (item,index) {
                        $("#tweetDisplay").prepend(constructTweet(item));
                    });
                }else{
                    console.log('No new tweets');
                }
            }else {
                console.log(status);
                console.log(data)
            }
        });
    };

    var constructTweet = function (data) {
        var tweet = "";
            tweet = "<div class='EmbeddedTweet'>" +
                "<div class='EmbeddedTweet-tweet'>" +
                    "<blockquote>" +
                        "<div class='Tweet-header'>" +
                            "<div class='Tweet-author'>" +
                "<a aria-label='"+data.user.screen_name+"'>" +
                "<span class='TweetAuthor-avatar'><img class='Avatar--edge' src=\""+data.user.profile_image_url+"\"></span>" +
                "<span class='Identity-name'>"+data.user.name+"</span>" +
                "<span class='TweetAuthor-screenName'>@"+data.user.screen_name+"</span>" +
                "<a/>" +
                            "</div>" +
                        "</div>" +
                        "<div class='Tweet-body'>" +
                        "<p class='Tweet-text'>"+ data.text+"</p>" +
                        "<time class='Tweet-text'>"+ data.created_at.substr(0,19)+"</time>" +
                        "</div>" +
                    "</blockquote>"+
                "</div>"+
                "</div>";
        return tweet;
    };

    var startTask = function() {
        var $name = $('#searchtxt').val();
        if ($name != "") {
            console.log('Starting watcher');
            //Changing info text
            $('#statusText').text('Now searching: "' + $name + '"');
            //Deleting previous data found
            $('#tweetDisplay').html("");
            searchTweets();
            refreshIntervalId = setInterval(searchTweets, 5000);
            watcherStatus = true;
        }else {
            console.log('Cannot start watcher without a search target');
            $('#statusText').text('Now searching: Nothing');
        }
    };

    var stopTask = function() {
        console.log('Stopping watcher');
        clearInterval(refreshIntervalId);
        $('#statusText').text('Now searching: Nothing');
        lastTweetId = 0;
    };

    // Action on pressing of button
    $(function(){
        $('#searchbtn').click(function() {
            startTask();
        });
    });

    // Action on pressing enter, stoping every search if anything else is pressed
    // could be more efficient if stopTask were not called every time
    $('#searchtxt').on("keyup", function(e) {
        if (e.keyCode == 13){ //on enter key press
            startTask();
        }else{
            if(watcherStatus == true){
                stopTask();
                watcherStatus = false;
            }
        }
    });

});