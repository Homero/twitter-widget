var gulp = require("gulp"),
    service = require("gulp-service"),
    sass = require("gulp-sass"),
    babelify = require("babelify"),
    browserify = require("browserify"),
    connect = require("gulp-connect"),
    source = require("vinyl-source-stream"),
    uglify = require("gulp-uglify"),
    buffer = require("vinyl-buffer");

gulp.task('buildhtml',function () {
    gulp.src('app/**/*.html')
        .pipe(gulp.dest('build'));
});

gulp.task('buildcss',function () {
    return gulp.src('app/scss/styles.scss')
        .pipe(sass({
            outputStyle: 'compressed'
        }))
        .pipe(gulp.dest('build/css'));
});

gulp.task('buildjs',function () {
    return browserify({
        entries: './app/js/index.js'
    }).transform(babelify.configure({
        presets: ['es2015']
    })).bundle()
        .pipe(source('bundle.js'))
        .pipe(buffer())
        .pipe(uglify())
        .pipe(gulp.dest('./build/js'));
});

gulp.task('startfront',function () {
    connect.server({
        root:'./build',
        livereload: true,
        port: 8085
    })
});

gulp.task('reloadhtml',['buildhtml'],function () {
    return gulp.src('app/**/*.html')
        .pipe(connect.reload());
});

gulp.task('reloadstyles',['buildcss'],function () {
    return gulp.src('app/**/*.scss')
        .pipe(connect.reload());
});

gulp.task('reloadjs',['buildjs'],function () {
    return gulp.src('app/**/*.js')
        .pipe(connect.reload());
});

gulp.task('buildall',['buildhtml','buildcss','buildjs'],function () {});

gulp.task('watcher',['buildall','startfront'],function () {
    gulp.watch('app/**/*.html',['reloadhtml']);
    gulp.watch('app/**/*.scss',['reloadstyles']);
    gulp.watch('app/**/*.js',['reloadjs']);
});

gulp.task('startback',function () {
    service.run('server/index.js',{
        env: {
            port: 8080
        }
    })
});

gulp.task('default',['startback','watcher']);